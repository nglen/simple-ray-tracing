mod camera;
mod color;
mod geometry;
mod nonnan;
pub mod prelude;
mod view;

use image::{ImageBuffer, Pixel, Rgb};
use nalgebra::{UnitQuaternion, Vector3};
use prelude::*;
use rayon::prelude::*;
use view::Light;

const WIDTH: u32 = 720;
const HEIGHT: u32 = 480;

fn main() {
    let scene = (|| {
        let ground = Plane
            .offset([0.0, 1.0, 0.0].into())
            .simple(GammaColor::GREEN.into(), 0.18);

        let sphere1 = sphere(1.0, [0.0, 0.0, 0.0].into()).simple(GammaColor::RED.into(), 0.18);
        let sphere2 = sphere(0.7, [0.5, 0.2, 2.0].into()).simple(GammaColor::YELLOW.into(), 0.18);
        let sphere3 = sphere(1.1, [-2.0, 0.0, -2.0].into()).simple(GammaColor::LIME.into(), 0.18);

        ground
            .union(vec![sphere1, sphere2, sphere3])
            .rotate(UnitQuaternion::from_euler_angles(0.0, 0.0, 0.0))
            .offset([0.0, 0.0, -5.0].into())
    })();

    let camera = Camera::new_wh(70.0, WIDTH, HEIGHT);
    let light = Light {
        dir: UnitQuaternion::face_towards(&[5.0, 10.0, -8.0].into(), &Vector3::z()),
        intensity: 1.0,
        color: GammaColor::WHITE.into(),
    };

    let img = par_image_from_fn(WIDTH, HEIGHT, |x, y| {
        let ray = camera.ray(x, y);
        let color = scene
            .trace(&ray, &light)
            .unwrap_or(GammaColor::WHITE.into())
            .clamp();

        GammaColor::from(color).to_rgb()
    });

    img.save("out.png").unwrap();
}

fn par_image_from_fn<F>(width: u32, height: u32, f: F) -> ImageBuffer<Rgb<u8>, Vec<u8>>
where
    F: Fn(f32, f32) -> Rgb<u8> + Sync,
{
    let mut buf = vec![0; 3 * width as usize * height as usize];
    buf.par_chunks_mut(3)
        .map(|v| Rgb::from_slice_mut(v))
        .enumerate()
        .for_each(|(idx, p)| {
            let x = (idx % width as usize) as f32 / width as f32;
            let y = (idx / width as usize) as f32 / height as f32;
            *p = f(x, y);
        });
    ImageBuffer::from_vec(width, height, buf).unwrap()
}
