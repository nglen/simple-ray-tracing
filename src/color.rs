use image::Rgb;

pub type Color = LinearColor;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct LinearColor {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct GammaColor {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

impl From<Rgb<u8>> for GammaColor {
    fn from(v: Rgb<u8>) -> GammaColor {
        GammaColor {
            r: f32::from(v[0]) / f32::from(u8::MAX),
            g: f32::from(v[1]) / f32::from(u8::MAX),
            b: f32::from(v[2]) / f32::from(u8::MAX),
        }
    }
}

impl From<LinearColor> for GammaColor {
    fn from(v: LinearColor) -> GammaColor {
        GammaColor {
            r: v.r.powf(GammaColor::GAMMA.recip()),
            g: v.g.powf(GammaColor::GAMMA.recip()),
            b: v.b.powf(GammaColor::GAMMA.recip()),
        }
    }
}

impl From<GammaColor> for LinearColor {
    fn from(v: GammaColor) -> LinearColor {
        LinearColor {
            r: v.r.powf(GammaColor::GAMMA),
            g: v.g.powf(GammaColor::GAMMA),
            b: v.b.powf(GammaColor::GAMMA),
        }
    }
}

#[allow(dead_code)]
impl GammaColor {
    pub const GAMMA: f32 = 2.2;

    pub const BLACK: Self = Self::new(0.0, 0.0, 0.0);
    pub const WHITE: Self = Self::new(1.0, 1.0, 1.0);

    pub const RED: Self = Self::new(1.0, 0.0, 0.0);
    pub const LIME: Self = Self::new(0.0, 1.0, 0.0);
    pub const BLUE: Self = Self::new(0.0, 0.0, 1.0);

    pub const YELLOW: Self = Self::new(1.0, 1.0, 0.0);
    pub const CYAN: Self = Self::new(0.0, 1.0, 1.0);
    pub const MAGENTA: Self = Self::new(1.0, 0.0, 1.0);

    pub const SILVER: Self = Self::new(0.75, 0.75, 0.75);

    pub const GRAY: Self = Self::new(0.5, 0.5, 0.5);
    pub const MAROON: Self = Self::new(0.5, 0.0, 0.0);
    pub const OLIVE: Self = Self::new(0.5, 0.5, 0.0);
    pub const GREEN: Self = Self::new(0.0, 0.5, 0.0);
    pub const PURPLE: Self = Self::new(0.5, 0.0, 0.5);
    pub const TEAL: Self = Self::new(0.0, 0.5, 0.5);
    pub const NAVY: Self = Self::new(0.0, 0.0, 0.5);

    pub const fn new(r: f32, g: f32, b: f32) -> Self {
        Self { r, g, b }
    }

    pub fn to_rgb(&self) -> Rgb<u8> {
        Rgb([
            (self.r * f32::from(u8::MAX)) as u8,
            (self.g * f32::from(u8::MAX)) as u8,
            (self.b * f32::from(u8::MAX)) as u8,
        ])
    }

    pub fn clamp(&self) -> Self {
        Self {
            r: f32::max(f32::min(self.r, 1.0), 0.0),
            g: f32::max(f32::min(self.g, 1.0), 0.0),
            b: f32::max(f32::min(self.b, 1.0), 0.0),
        }
    }
}

impl LinearColor {
    pub fn lerp(&self, other: &Self, time: f32) -> Self {
        let time = f32::max(f32::min(time, 1.0), 0.0);
        Self {
            r: self.r + (other.r - self.r) * time,
            g: self.g + (other.g - self.g) * time,
            b: self.b + (other.b - self.b) * time,
        }
    }

    pub fn clamp(&self) -> Self {
        Self {
            r: f32::max(f32::min(self.r, 1.0), 0.0),
            g: f32::max(f32::min(self.g, 1.0), 0.0),
            b: f32::max(f32::min(self.b, 1.0), 0.0),
        }
    }
}
