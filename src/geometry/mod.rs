mod plane;
mod ray;
mod sphere;

pub use self::plane::Plane;
pub use self::ray::Ray;
pub use self::sphere::Sphere;
use crate::nonnan::NonNan;
use crate::view::CombinatorHelper;
use either::Either;
use nalgebra::{Point3, Unit, Vector3};
use std::convert::TryFrom;
use std::ops::Deref;

pub trait Intersection {
    fn dist(&self) -> f32;

    fn normal(&self) -> Unit<Vector3<f32>>;

    fn hit_point(&self, ray: &Ray) -> Point3<f32>;
}

pub trait Intersect {
    type Intersection: Intersection;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection>;
}

pub fn sphere(radius: f32, center: Point3<f32>) -> impl Intersect {
    let off = center - Point3::origin();
    Sphere.scale(radius).offset(off)
}

impl<L: Intersection, R: Intersection> Intersection for Either<L, R> {
    fn dist(&self) -> f32 {
        match self {
            Self::Left(v) => v.dist(),
            Self::Right(v) => v.dist(),
        }
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        match self {
            Self::Left(v) => v.normal(),
            Self::Right(v) => v.normal(),
        }
    }

    fn hit_point(&self, ray: &Ray) -> Point3<f32> {
        match self {
            Self::Left(v) => v.hit_point(ray),
            Self::Right(v) => v.hit_point(ray),
        }
    }
}

impl<T, U> Intersect for T
where
    T: Deref<Target = [U]>,
    U: Intersect,
{
    type Intersection = U::Intersection;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        self.iter()
            .filter_map(|v| v.intersects(ray))
            .min_by_key(|v| NonNan::try_from(v.dist()).unwrap())
    }
}
