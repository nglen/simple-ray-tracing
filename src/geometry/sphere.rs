use super::{Intersect, Intersection, Ray};
use nalgebra::{Point3, Unit, Vector3};

pub struct Sphere;
pub struct SphereInter(f32, Point3<f32>);

impl Intersect for Sphere {
    type Intersection = SphereInter;

    // http://kylehalladay.com/blog/tutorial/math/2013/12/24/Ray-Sphere-Intersection.html
    fn intersects(&self, ray: &Ray) -> Option<SphereInter> {
        let dir = ray.dir * Vector3::z();

        let l = Point3::origin() - ray.origin;
        let tc = l.dot(&dir);

        if tc < 0.0 {
            return None;
        }
        let d2 = l.dot(&l) - (tc * tc);

        let r2 = 1.0;
        if d2 > r2 {
            return None;
        }

        let t1c = (r2 - d2).sqrt();

        // The two collision points are tc ± t1c.
        // We want the smallest and since t1c > 0, this means we subtract it.
        let dist = tc - t1c;
        let point = ray.origin + dir * dist;
        Some(SphereInter(dist, point))
    }
}

impl Intersection for SphereInter {
    fn dist(&self) -> f32 {
        self.0
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        Unit::new_normalize(self.1 - Point3::origin())
    }

    fn hit_point(&self, _ray: &Ray) -> Point3<f32> {
        self.1
    }
}
