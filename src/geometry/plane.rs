use super::{Intersect, Intersection, Ray};
use nalgebra::{Point3, Unit, Vector3};

pub struct Plane;
pub struct PlaneInter(bool, f32);

impl Intersect for Plane {
    type Intersection = PlaneInter;

    fn intersects(&self, ray: &Ray) -> Option<PlaneInter> {
        let dir = ray.dir * Vector3::z();

        if ray.origin.y > 0.0 && dir.y < 0.0 {
            Some(PlaneInter(true, -ray.origin.y / dir.y))
        } else if ray.origin.y < 0.0 && dir.y > 0.0 {
            Some(PlaneInter(false, -ray.origin.y / dir.y))
        } else {
            None
        }
    }
}

impl Intersection for PlaneInter {
    fn dist(&self) -> f32 {
        self.1
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        if self.0 {
            Vector3::y_axis()
        } else {
            -Vector3::y_axis()
        }
    }

    fn hit_point(&self, ray: &Ray) -> Point3<f32> {
        ray.origin + ray.dir * Vector3::z() * self.dist()
    }
}
