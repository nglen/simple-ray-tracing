use nalgebra::{Point3, UnitQuaternion};

#[derive(Debug)]
pub struct Ray {
    pub origin: Point3<f32>,
    pub dir: UnitQuaternion<f32>,
}
