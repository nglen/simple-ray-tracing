use std::cmp::Ordering;
use std::convert::TryFrom;

#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Hash)]
pub struct NonNan<T>(T);

impl Eq for NonNan<f32> {}
impl Ord for NonNan<f32> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).expect("NonNan had a value of NaN")
    }
}

impl TryFrom<f32> for NonNan<f32> {
    type Error = &'static str;

    fn try_from(v: f32) -> Result<Self, Self::Error> {
        if v.is_nan() {
            Err("NonNan cannot have a value of NaN")
        } else {
            Ok(Self(v))
        }
    }
}
