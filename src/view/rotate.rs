use super::HitView;
use crate::color::Color;
use crate::geometry::{Intersect, Intersection, Ray};
use nalgebra::{Point3, Unit, UnitQuaternion, Vector3};

impl<T> RotateView<T> {
    fn transform_ray(&self, ray: &Ray) -> Ray {
        Ray {
            origin: self.rot * ray.origin,
            dir: self.rot * ray.dir,
        }
    }
}

pub struct RotateView<T> {
    pub rot: UnitQuaternion<f32>,
    pub inner: T,
}

impl<T: Intersection> Intersection for RotateView<T> {
    fn dist(&self) -> f32 {
        self.inner.dist()
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        self.rot.inverse() * self.inner.normal()
    }

    fn hit_point(&self, ray: &Ray) -> Point3<f32> {
        self.rot.inverse() * self.inner.hit_point(&self.transform_ray(ray))
    }
}

impl<T: HitView> HitView for RotateView<T> {
    fn color(&self) -> Color {
        self.inner.color()
    }

    fn albedo(&self) -> f32 {
        self.inner.albedo()
    }
}

impl<T: Intersect> Intersect for RotateView<T> {
    type Intersection = RotateView<T::Intersection>;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        self.inner
            .intersects(&self.transform_ray(ray))
            .map(|inner| RotateView {
                rot: self.rot,
                inner,
            })
    }
}
