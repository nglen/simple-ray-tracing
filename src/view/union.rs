use crate::geometry::{Intersect, Intersection, Ray};
use either::Either;

pub struct UnionView<L, R> {
    pub left: L,
    pub right: R,
}

impl<L: Intersect, R: Intersect> Intersect for UnionView<L, R> {
    type Intersection = Either<L::Intersection, R::Intersection>;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        let p1 = self.left.intersects(ray);
        let p2 = self.right.intersects(ray);

        match (p1, p2) {
            (Some(p1), Some(p2)) if p1.dist() > p2.dist() => Some(Either::Right(p2)),
            (Some(p1), _) => Some(Either::Left(p1)),
            (_, Some(p2)) => Some(Either::Right(p2)),
            (None, None) => None,
        }
    }
}
