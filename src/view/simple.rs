use super::HitView;
use crate::color::Color;
use crate::geometry::{Intersect, Intersection, Ray};
use nalgebra::{Point3, Unit, Vector3};

pub struct SimpleView<T> {
    pub color: Color,
    pub albedo: f32,
    pub inner: T,
}

impl<T> HitView for SimpleView<T> {
    fn color(&self) -> Color {
        self.color
    }

    fn albedo(&self) -> f32 {
        self.albedo
    }
}

impl<T: Intersection> Intersection for SimpleView<T> {
    fn dist(&self) -> f32 {
        self.inner.dist()
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        self.inner.normal()
    }

    fn hit_point(&self, ray: &Ray) -> Point3<f32> {
        self.inner.hit_point(ray)
    }
}

impl<T: Intersect> Intersect for SimpleView<T> {
    type Intersection = SimpleView<T::Intersection>;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        self.inner.intersects(ray).map(|inner| SimpleView {
            color: self.color,
            albedo: self.albedo,
            inner,
        })
    }
}
