use crate::geometry::{Intersect, Ray};
use either::Either;

pub struct OrView<L, R> {
    pub left: L,
    pub right: R,
}

impl<L: Intersect, R: Intersect> Intersect for OrView<L, R> {
    type Intersection = Either<L::Intersection, R::Intersection>;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        self.left
            .intersects(ray)
            .map(Either::Left)
            .or_else(|| self.right.intersects(ray).map(Either::Right))
    }
}
