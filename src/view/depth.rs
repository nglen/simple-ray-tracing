use super::HitView;
use crate::color::Color;
use crate::geometry::{Intersect, Intersection, Ray};
use nalgebra::{Point3, Unit, Vector3};

pub struct DepthView<T> {
    pub near: Color,
    pub far: Color,
    pub center: f32,
    pub scale: f32,
    pub inner: T,
}

impl<T: Intersection> HitView for DepthView<T> {
    fn color(&self) -> Color {
        let scaled = (self.inner.dist() - self.center) / self.scale;

        // TANH
        let t = scaled.tanh() / 2.0 + 0.5;
        assert!(t >= 0.0);

        self.near.lerp(&self.far, t)
    }

    fn albedo(&self) -> f32 {
        0.18
    }
}

impl<T: Intersection> Intersection for DepthView<T> {
    fn dist(&self) -> f32 {
        self.inner.dist()
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        self.inner.normal()
    }

    fn hit_point(&self, ray: &Ray) -> Point3<f32> {
        self.inner.hit_point(ray)
    }
}

impl<T: Intersect> Intersect for DepthView<T> {
    type Intersection = DepthView<T::Intersection>;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        self.inner.intersects(ray).map(|inner| DepthView {
            near: self.near,
            far: self.far,
            center: self.center,
            scale: self.scale,
            inner,
        })
    }
}
