use super::HitView;
use crate::color::Color;
use crate::geometry::{Intersect, Intersection, Ray};
use nalgebra::{Point3, Unit, Vector3};

pub struct TranslateView<T> {
    pub off: Vector3<f32>,
    pub inner: T,
}

impl<T> TranslateView<T> {
    fn transform_ray(&self, ray: &Ray) -> Ray {
        Ray {
            origin: ray.origin + self.off,
            dir: ray.dir,
        }
    }
}

impl<T: Intersection> Intersection for TranslateView<T> {
    fn dist(&self) -> f32 {
        self.inner.dist()
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        self.inner.normal()
    }

    fn hit_point(&self, ray: &Ray) -> Point3<f32> {
        self.inner.hit_point(&self.transform_ray(&ray)) - self.off
    }
}

impl<T: HitView> HitView for TranslateView<T> {
    fn color(&self) -> Color {
        self.inner.color()
    }

    fn albedo(&self) -> f32 {
        self.inner.albedo()
    }
}

impl<T: Intersect> Intersect for TranslateView<T> {
    type Intersection = T::Intersection;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        self.inner.intersects(&self.transform_ray(&ray))
    }
}
