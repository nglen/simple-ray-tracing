mod depth;
mod normal;
mod or;
mod rotate;
mod scale;
mod simple;
mod translate;
mod union;

use self::{
    depth::DepthView, normal::NormalView, or::OrView, rotate::RotateView, scale::ScaleView,
    simple::SimpleView, translate::TranslateView, union::UnionView,
};
use crate::color::Color;
use crate::geometry::{Intersect, Intersection, Ray};
use either::Either;
use nalgebra::{UnitQuaternion, Vector3};

pub trait HitView {
    fn color(&self) -> Color;
    fn albedo(&self) -> f32;
}

impl<L: HitView, R: HitView> HitView for Either<L, R> {
    fn color(&self) -> Color {
        match self {
            Self::Left(v) => v.color(),
            Self::Right(v) => v.color(),
        }
    }

    fn albedo(&self) -> f32 {
        match self {
            Self::Left(v) => v.albedo(),
            Self::Right(v) => v.albedo(),
        }
    }
}

pub trait CombinatorHelper: Sized {
    fn offset(self, off: Vector3<f32>) -> TranslateView<Self> {
        TranslateView { off, inner: self }
    }

    fn rotate(self, rot: UnitQuaternion<f32>) -> RotateView<Self> {
        RotateView { rot, inner: self }
    }

    fn scale(self, scale: f32) -> ScaleView<Self> {
        ScaleView { scale, inner: self }
    }

    fn or_else<T>(self, right: T) -> OrView<Self, T> {
        OrView { left: self, right }
    }

    fn union<T>(self, right: T) -> UnionView<Self, T> {
        UnionView { left: self, right }
    }

    fn depth(self, center: f32, scale: f32, near: Color, far: Color) -> DepthView<Self> {
        DepthView {
            near,
            far,
            center,
            scale,
            inner: self,
        }
    }

    fn normals(self) -> NormalView<Self> {
        NormalView { inner: self }
    }

    fn simple(self, color: Color, albedo: f32) -> SimpleView<Self> {
        SimpleView {
            color,
            albedo,
            inner: self,
        }
    }
}

impl<T> CombinatorHelper for T where T: Intersect + Sized {}

pub struct Light {
    pub dir: UnitQuaternion<f32>,
    pub intensity: f32,
    pub color: Color,
}

pub trait View: Intersect {
    fn trace(&self, ray: &Ray, light: &Light) -> Option<Color>;
}

impl<T> View for T
where
    T: Intersect,
    T::Intersection: HitView,
{
    fn trace(&self, ray: &Ray, light: &Light) -> Option<Color> {
        let inter = self.intersects(ray)?;

        let in_light = self
            .intersects(&Ray {
                origin: inter.hit_point(ray) + inter.normal().into_inner() * 0.0001,
                dir: light.dir,
            })
            .is_none();

        let light_dir = light.dir * Vector3::z();
        let light_intensity = if in_light { light.intensity } else { 0.0 };
        let light_power = f32::max(inter.normal().dot(&light_dir) as f32 * light_intensity, 0.0);
        let light_reflected = inter.albedo() / std::f32::consts::PI;
        let color = inter.color();

        Some(Color {
            r: color.r * light.color.r * light_power * light_reflected,
            g: color.g * light.color.g * light_power * light_reflected,
            b: color.b * light.color.b * light_power * light_reflected,
        })
    }
}
