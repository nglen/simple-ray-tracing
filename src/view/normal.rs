use super::HitView;
use crate::color::{Color, GammaColor};
use crate::geometry::{Intersect, Intersection, Ray};
use nalgebra::{Point3, Unit, Vector3};

pub struct NormalView<T> {
    pub inner: T,
}

impl<T: Intersection> HitView for NormalView<T> {
    fn color(&self) -> Color {
        let normal = self.normal();
        let r = normal.x / 2.0 + 0.5;
        let g = normal.y / 2.0 + 0.5;
        let b = normal.z / 2.0 + 0.5;

        (GammaColor { r, g, b }).into()
    }

    fn albedo(&self) -> f32 {
        0.18
    }
}

impl<T: Intersection> Intersection for NormalView<T> {
    fn dist(&self) -> f32 {
        self.inner.dist()
    }

    fn normal(&self) -> Unit<Vector3<f32>> {
        self.inner.normal()
    }

    fn hit_point(&self, ray: &Ray) -> Point3<f32> {
        self.inner.hit_point(ray)
    }
}

impl<T: Intersect> Intersect for NormalView<T> {
    type Intersection = NormalView<T::Intersection>;

    fn intersects(&self, ray: &Ray) -> Option<Self::Intersection> {
        self.inner.intersects(ray).map(|inner| NormalView { inner })
    }
}
