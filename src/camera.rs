use crate::Ray;
use nalgebra::{Point3, UnitQuaternion};

pub struct Camera {
    fov: f32,
    aspect_ratio: f32,
}

impl Camera {
    pub fn new(fov: f32, aspect_ratio: f32) -> Self {
        Self { fov, aspect_ratio }
    }

    pub fn new_wh(fov: f32, width: u32, height: u32) -> Self {
        Self::new(fov, width as f32 / height as f32)
    }

    pub fn hfov(&self) -> f32 {
        self.fov
    }

    pub fn vfov(&self) -> f32 {
        self.fov / self.aspect_ratio
    }

    pub fn ray(&self, x: f32, y: f32) -> Ray {
        let horz_angle = (x - 0.5) * self.hfov();
        let vert_angle = (y - 0.5) * self.vfov();

        let rot = UnitQuaternion::from_euler_angles(
            vert_angle.to_radians(),
            horz_angle.to_radians(),
            0.0,
        );

        Ray {
            origin: Point3::origin(),
            dir: rot,
        }
    }
}
